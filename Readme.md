

<!-- Readme.md is generated from Readme.org. Please edit that file -->


# limix R package

This package makes some functionality of `LIMIX` available in `R`.

From the [LIMIX repository](https://github.com/PMBio/limix) :

> \`LIMIX\` is a flexible and efficient linear mixed model library.

Right now the only function allows fitting of mixed GP regression model and predicting from it.

## Example Usage

This is the example from the help page of `mixedGPmodel_limix` (see
with `?mixedGPmodel_limix`).

First, we generate some data:

```R
## samples
N <- 400

## noise level
v_noise <- 0.01

## data
## predictor
X <- seq(0, 2, length.out = N)
## response
Y <- sin(X) + sqrt(v_noise) * rnorm(N)
## covariate
C <- sample(c("X", "Y", "Z"), N, replace=TRUE)
## put it all together
df <- data.frame(x=X, y=Y, c=C)
```

Now, let's fit a GP regression model with `LIMIX`:

```R
library(limix)

## fit the model
fit <- mixedGPmodel_limix(y="y", fixed_effects=c("c"), random_effects="x", data=df)
```

    Marginal likelihood optimization.
    Converged: True
    Time elapsed: 2.08 s
    Log Marginal Likelihood: -247.6888712.
    Gradient norm: 0.0001092.
    Standard errors calculation.

We can have a look at the parameters:

```R
## weights of fixed effects
fit$fixed_weights
```

<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-left" />

<col  class="org-right" />

<col  class="org-right" />
</colgroup>
<thead>
<tr>
<th scope="col" class="org-left">parameter</th>
<th scope="col" class="org-right">estimate</th>
<th scope="col" class="org-right">ste</th>
</tr>
</thead>

<tbody>
<tr>
<td class="org-left">cY</td>
<td class="org-right">-0.0989423952678068</td>
<td class="org-right">0.0382920344457835</td>
</tr>


<tr>
<td class="org-left">cZ</td>
<td class="org-right">-0.0431845383401536</td>
<td class="org-right">0.0391383539897773</td>
</tr>
</tbody>
</table>

```R
## optimized parameters
fit$fit_parameters
```

<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-left" />

<col  class="org-right" />

<col  class="org-right" />
</colgroup>
<thead>
<tr>
<th scope="col" class="org-left">parameter</th>
<th scope="col" class="org-right">value</th>
<th scope="col" class="org-right">ste</th>
</tr>
</thead>

<tbody>
<tr>
<td class="org-left">sqexp.scale</td>
<td class="org-right">4.01914099111708</td>
<td class="org-right">2.88791131630081</td>
</tr>


<tr>
<td class="org-left">sqexp.length</td>
<td class="org-right">4.67374170818022</td>
<td class="org-right">0.374919210019683</td>
</tr>


<tr>
<td class="org-left">noise.scale</td>
<td class="org-right">0.0971969265694726</td>
<td class="org-right">0.00691271791808271</td>
</tr>
</tbody>
</table>

Finally, we visualize the result:

```R
## have a look
plot(X, Y)
lines(X, fit$Ystar, col="darkred")
lines(X, sin(X))
```

![img](README-simple_example-1.png)

## Installation

Installation directly from gitlab is possible, when the package `devtools`
is installed.

LIMIX is required as a system dependency.  Install via

```shell
pip install limix
```

As R dependencies you'll need the [rPython](https://cran.r-project.org/web/packages/rPython/index.html) and the [rPythonConvenienceFunctions](https://gitlab.com/al22/rPythonConvenienceFunctions) packages.  To install these run

```R
install.packages("rPython")
library("devtools")
install_git("https://gitlab.com/al22/rPythonConvenienceFunctions.git")
```

Once all dependencies are installed, get this package with

```R
library("devtools")
install_git("https://gitlab.com/al22/r-limix.git")
```

## Roadmap

This package is a first step in making `LIMIX` functionality
accessible from `R` directly.  There will be always work to provide
further functions in `R`.  These are just the next steps that are on
the agenda:

-   formula interface for `mixedGPmodel_limix`
-   interface for variance components

    Here the data exchange is not clear to me yet.  `LIMIX` expects data
    to be read from disk via hdf5 files.  So, one route might be to
    write the data in the appropriate form to a temp file and read it
    back into python.  Seems very indirect compared to the JSON route
    taken by `rPython`.  But probably the easier thing to do.


<!--
Local Variables:
 mode: gfm
 markdown-command: "marked"
End:
-->
